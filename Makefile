CC = gcc
CFLAGS = -Wall -Werror -Wextra -g

NAME = minishell
SRCS = lexer1.c \
		print.c \
		# main.c \
		# list.c \
		# lexer.c \
		# split_str.c
OBJS = $(SRCS:.c=.o)

LIBFT_DIR = libft
LIBFT_NAME = libft.a
LIBS = -L$(LIBFT_DIR) -lft -lreadline

all: $(LIBFT_DIR)/$(LIBFT_NAME) $(NAME)

$(NAME): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(NAME) $(LIBS)

$(LIBFT_DIR)/$(LIBFT_NAME):
	make -sC $(LIBFT_DIR)

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	make -sC $(LIBFT_DIR) clean
	rm -rf $(OBJS)

fclean: clean
	make -sC $(LIBFT_DIR) fclean
	rm -rf $(NAME)

re: fclean all

.PHONY: all clean fclean re