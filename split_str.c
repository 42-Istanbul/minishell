#include "minishell.h"

static void	ft_allocate(char **tab, char const *s)
{
	char		**tab_p;
	char const	*tmp;

	tmp = s;
	tab_p = tab;
	while (*tmp)
	{
		while (*s == ' ')
			++s;
		tmp = s;
		while (*tmp && !ft_strchr(SPECIAL, *tmp))
			++tmp;
		if (tmp > s)
		{
			*tab_p = ft_substr(s, 0, tmp - s);
			s = tmp;
			++tab_p;
		}
	}
	*tab_p = NULL;
}

static int	ft_ccount_words(char const *s)
{
	int	word_count;

	word_count = 0;
	while (*s)
	{
		while (*s == ' ')
			++s;
		if (*s && !ft_strchr(SPECIAL, *s))
			++word_count;
		if (ft_strchr(SPECIAL, *s))
		{
			++word_count;
			++s;
		}
		while (*s && *s != ' ')
			++s;
	}
	return (word_count);
}

char	**ft_csplit(char const *s)
{
	char	**new;
	int		size;

	if (!s)
		return (NULL);
	size = ft_ccount_words(s);
	new = (char **)malloc(sizeof(char *) * (size + 1));
	if (!new)
		return (NULL);
	ft_allocate(new, s);
	return (new);
}
