#ifndef MINISHELL_H
# define MINISHELL_H

# include <stdio.h>
# include <stdlib.h>
# include <readline/readline.h>
# include <readline/history.h>
# include "./libft/includes/libft.h"

# define SPECIAL "<>|&$ \t()"

typedef struct  s_arg
{
  struct s_arg *next;
  char          **arr;
  int           flag;
}              t_arg;

typedef struct  s_my_env
{
  char **env;
  char **exp;
}              t_my_env; 

void	ft_print2d(char **s);
void	ft_printlist(t_arg *head);

char	*single_quote(char *s, int *i);
char	*double_quote(char *s, int *i);

char	**ft_csplit(char const *s);

t_arg	*ft_argnew(char **s);
t_arg	*ft_arglast(t_arg *head);
t_arg	*ft_argbeforelast(t_arg *head);
int	  ft_argsize(t_arg *head);
void	ft_argadd_back(t_arg **head, t_arg *new);
void	ft_argadd_front(t_arg **head, t_arg *new);
void	ft_argclear(t_arg **head);

char  *lexer(char *s, char **env);

#endif
