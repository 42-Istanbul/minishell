#include "minishell.h"

t_arg	*ft_argnew(char **s)
{
	t_arg	*new;

	new = (t_arg *)malloc(sizeof(t_arg));
	if (!new)
		return (NULL);
	new->arr = s;
	new->flag = 0;
	new->next = NULL;
	return (new);
}

t_arg	*ft_arglast(t_arg *head)
{
	while (head->next)
		head = head->next;
	return (head);
}

t_arg	*ft_argbeforelast(t_arg *head)
{
	while (head->next->next)
		head = head->next;
	return (head);
}

int	ft_argsize(t_arg *head)
{
	int	i;

	i = 0;
	while (head)
	{
		head = head->next;
		++i;
	}
	return (i);
}

void	ft_argadd_back(t_arg **head, t_arg *new)
{
	t_arg	*tmp;

	if (*head == NULL)
	{
		*head = new;
		return ;
	}
	tmp = *head;
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = new;
}

void	ft_argadd_front(t_arg **head, t_arg *new)
{
	new->next = *head;
	*head = new;
}

void	ft_argclear(t_arg **head)
{
	t_arg	*tmp;

	while (*head)
	{
		tmp = *head;
		*head = (*head)->next;
		free(tmp);
	}
}
