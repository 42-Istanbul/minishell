#include "minishell.h"

char	*single_quote(char *s, int *i)
{
	int		j;
	char	*tmp1;
	char	*tmp2;
	char	*tmp3;

	j = *i;
	while (s[++(*i)])
		if (s[*i] == '\'')
			break;
	tmp1 = ft_substr(s, 0, j);
	tmp2 = ft_substr(s, j + 1, *i - j - 1);
	tmp3 = ft_strdup(s + *i + 1);
	tmp1 = ft_strjoin(tmp1, tmp2);
	free(tmp2);
	tmp1 = ft_strjoin(tmp1, tmp3);
	free(tmp3);
	ft_printf("tmp sq = %s\n", tmp1);
	free(s);
	return (tmp1);
}

char	*double_quote(char *s, int *i)
{
	int		j;
	char	*tmp1;
	char	*tmp2;
	char	*tmp3;

	j = *i;
	while (s[++(*i)])
		if (s[*i] == '\"')
			break;
	tmp1 = ft_substr(s, 0, j);
	tmp2 = ft_substr(s, j + 1, *i - j - 1);
	tmp3 = ft_strdup(s + *i + 1);
	tmp1 = ft_strjoin(tmp1, tmp2);
	free(tmp2);
	tmp1 = ft_strjoin(tmp1, tmp3);
	free(tmp3);
	ft_printf("tmp dq = %s\n", tmp1);
	free(s);
	return (tmp1);
}

char	*dollar(char *s, int *i, char **env)
{
	int		j;
	int		k;
	int		z;
	char	*tmp1;
	char	*tmp2;

	j = *i;
	while (s[++(*i)])
		if (s[*i] != '_' && !ft_isalnum(s[*i]))
			break;
	if (*i == j + 1)
		return (s);
	tmp1 = ft_substr(s, j + 1, *i - j - 1);
	k = -1;
	while (env[++k])
	{
		if (strstr(env[k], tmp1))
		{
			z = 0;
			while (env[k][z] && env[k][z] != '=')
				++z;
			tmp2 = ft_substr(env[k], 0, z);
			if (ft_strncmp(tmp1, tmp2, 100) == 0)
				break ;
		}
	}
	tmp2 = ft_substr(env[k], z + 1, strlen(env[k]) - z);
	ft_printf("s = %s\ntmp1 = %s\ntmp2 = %s\n", s, tmp1, tmp2);
	return (s);
}

char	*lexer(char *s, char **env)
{
	int	i;

	i = -1;
	while (s[++i])
	{
		if (s[i] == '\'')
			s = single_quote(s, &i);
		if (s[i] == '\"')
			s = double_quote(s, &i);
		if (s[i] == '$')
			s = dollar(s, &i, env);
	}
	return (s);
}
