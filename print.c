#include "minishell.h"

void	ft_print2d(char **s)
{
	char	**p;

	if (s)
	{
		p = s;
		ft_printf("[");
		while (*p)
		{
			ft_printf("\"%s\"", *p);
			if (*(p + 1))
				ft_printf(", ");
			++p;
		}
		ft_printf("]\n");
	}
}

void	ft_printlist(t_arg *head)
{
	while (head)
	{
		ft_print2d(head->arr);
		ft_printf("->");
		head = head->next;
	}
	ft_printf("(null)\n");
}
