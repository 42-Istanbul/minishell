#include "minishell.h"

int	count_args(char *s)
{
	char	*p;
	int		i;

	i = 0;
	p = s;
	while (p)
	{
		p = ft_strnstr(p, "||", 1000);
		if (p)
			p += 2;
		++i;
	}
	p = s;
	while (p)
	{
		p = ft_strnstr(p, "&&", 1000);
		if (p)
		{
			p += 2;
			++i;
		}
	}
	return (i);
}

char	**split_args(char *s)
{
	char	**out;
	int		len;

	out = NULL;
	len = count_args(s);
	out = (char **)malloc(sizeof(char *) * (len + 1));
	out[len] = NULL;
	printf("%d\n", len);
	(void)len;
	return (out);
}

int	main(void)
{
	char **p;

	char *s = "ls cat | ls || ls -la";
	p = split_args(s);
	ft_print2d(p);
	return (0);
}
